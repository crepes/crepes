# CVA6 RISC-V Etude Pour Etablir une Shadow stack



## Presentation

This project has been led within CentraleSupelec between February 2023 and June 2023. It's purposes was  to implement a POC of a shadow stack for CVA6.

## State of the project
We were able to develop and an [exploit](https://gitlab-research.centralesupelec.fr/crepes/exploits) to show the need for a shadow stack. We also developed a [detection system for ra operation](https://gitlab-research.centralesupelec.fr/crepes/custom_cva6/-/tree/detect-ra-ops?ref_type=heads) and a [minimal shadow stack on one register](https://gitlab-research.centralesupelec.fr/crepes/custom_cva6/-/tree/reg-shadow-stack?ref_type=heads)
